directory.BusquedaView = Backbone.View.extend({

    initialize: function(){
        return this;
    },
    renderList : function(col){
        this.$el.html(this.template());
        col.each(function(e){
            e.renderDatedExcerptTo("busqueda-post-list");
        });
        return this;
    },
    renderSearchList: function(query){
        var self = this;
        var col = new (directory.SearchPostCollection(query))();
        col.fetch({success:function(result){
            self.renderList(result);
        }});
    },
    render: function () {
        RenderTwitter();
        return this;
    }
});