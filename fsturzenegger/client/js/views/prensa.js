directory.PrensaView = Backbone.View.extend({
    render: function () {
        this.$el.html(this.template());
        loadSlider("slider-prensa","slider-prensa");
        jQuery("#navigation-images a").show();
        jQuery("#prensa-navigation-box").hide();

        new directory.PostsPrensa().fetch({
            success: function (self) {
                self.each(function(aPost){
                    aPost.renderDatedExcerptTo("prensa-post-list");
                });
            }
        });

        HideTwitterFeedAndShowNavigation();
        return this;
    }
});
