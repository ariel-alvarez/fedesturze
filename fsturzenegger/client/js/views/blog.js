directory.BlogView = Backbone.View.extend({

    render: function () {
        var self = this;
        this.$el.html(this.template());
        jQuery("#navigation-images").find("a").show();
        jQuery("#blog-navigation-box").hide();
        loadSlider("slider-blog","slider-blog");

        var numeroDePagina= Number(findUrlParameter("pagina"));

        PostsHome.findByCategories([Categorias.BLOG], function(col){
            self.posts = col;
            self.pages = paginar(col);
            if(numeroDePagina <= self.pages.length){
                self.renderPage(numeroDePagina);
            }

            var paginator = $("#blog-paginator");
            paginator.append($('<li><a href="#">&laquo;</a></li>'));
            _(self.pages).each(function(p, i){
                paginator.append($('<li><a href="#blog?pagina='+i+'">'+ (i+1) +'</a></li>').addClass(numeroDePagina == i? "pagina-seleccionada":""));
            });
            paginator.append($('<li><a href="#">&raquo;</a></li>'));
        });

        HideTwitterFeedAndShowNavigation();
        return this;
    },
    renderPage:function(index){
        _( this.pages[index]).each(function(aPost){
            aPost.renderDatedExcerptTo("blog-post-list");
        });
    }

});

function paginar(col){
    var paginas = [];
    col = col.toArray();
    console.log(">>> " + col.length);
    for (i=0,j=col.length; i<j; i+=MAX_POSTS) {
        paginas.push(col.slice(i,i+MAX_POSTS));
    }
    return paginas;
}